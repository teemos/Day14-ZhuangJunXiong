# ORID

## O

- This morning, I reviewed yesterday's front-end task. Due to the low difficulty of the code, my focus was on responsive layout. However, I did a poor job in this regard. The interface I created looked like a responsive layout, but after a few clicks, there was a problem. It was a pseudo responsive layout because I had never written any relevant code before.
- Yesterday was the backend API for front-end call simulation. Today, we wrote a complete backend and used a real API to complete front-end to backend calls. As it is a familiar thing, it is not difficult. It is equivalent to reviewing last week's content to deepen our impression.

## R

Good

## I

I am not familiar with responsive layout. I worked on it for a while last night and thought it was a success, but I still made a mistake.

## D

I will take the time to learn the specific principles of responsive layout.