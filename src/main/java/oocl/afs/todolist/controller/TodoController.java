package oocl.afs.todolist.controller;

import oocl.afs.todolist.service.TodoService;
import oocl.afs.todolist.service.dto.TodoCreateRequest;
import oocl.afs.todolist.service.dto.TodoResponse;
import oocl.afs.todolist.service.mapper.TodoMapper;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/todos")
public class TodoController {
    private final TodoService todoService;

    public TodoController(TodoService todoService) {
        this.todoService = todoService;
    }

    @GetMapping
    public List<TodoResponse> getAllTodos() {
        return todoService.findAll();
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteTodo(@PathVariable Long id) {
        todoService.delete(id);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public TodoResponse updateTodo(@PathVariable Long id, @RequestBody TodoCreateRequest todoCreateRequest) {
        return todoService.update(id, todoCreateRequest);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public TodoResponse createTodo(@RequestBody TodoCreateRequest todoCreateRequest) {
        return todoService.create(TodoMapper.toEntity(todoCreateRequest));
    }

    @GetMapping("/{id}")
    public TodoResponse getTodoById(@PathVariable Long id) {
        return todoService.findById(id);
    }
}
