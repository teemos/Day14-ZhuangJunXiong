package oocl.afs.todolist.service;

import oocl.afs.todolist.entity.Todo;
import oocl.afs.todolist.repository.TodoJPARepository;
import oocl.afs.todolist.service.dto.TodoCreateRequest;
import oocl.afs.todolist.service.dto.TodoResponse;
import oocl.afs.todolist.service.mapper.TodoMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import oocl.afs.todolist.exception.TodoNotFoundException;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TodoService {
    private final TodoJPARepository todoJPARepository;

    public TodoService(TodoJPARepository todoJPARepository) {
        this.todoJPARepository = todoJPARepository;
    }

    public List<TodoResponse> findAll() {
        return todoJPARepository.findAll()
                .stream()
                .map(TodoMapper::toResponse)
                .collect(Collectors.toList());
    }

    public void delete(Long id) {
        todoJPARepository.deleteById(id);
    }

    public TodoResponse findById(Long id) {
        Todo employee = todoJPARepository.findById(id)
                .orElseThrow(TodoNotFoundException::new);
        return TodoMapper.toResponse(employee);
    }

    public TodoResponse update(Long id, TodoCreateRequest todo) {
        TodoResponse toBeUpdatedTodo = findById(id);
        Todo TodoTemp = new Todo();
        BeanUtils.copyProperties(toBeUpdatedTodo, TodoTemp);
        if (todo.getName() != null) {
            TodoTemp.setName(todo.getName());
        }
        if (todo.getDone() != null) {
            TodoTemp.setDone(todo.getDone());
        }

        Todo saveTodo = todoJPARepository.save(TodoTemp);
        return TodoMapper.toResponse(saveTodo);
    }

    public TodoResponse create(Todo todo) {
        Todo savedTodo = todoJPARepository.save(todo);
        return TodoMapper.toResponse(savedTodo);
    }
}
