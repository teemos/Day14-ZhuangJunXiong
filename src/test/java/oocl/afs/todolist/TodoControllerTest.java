package oocl.afs.todolist;

import com.fasterxml.jackson.databind.ObjectMapper;
import oocl.afs.todolist.entity.Todo;
import oocl.afs.todolist.repository.TodoJPARepository;
import oocl.afs.todolist.service.dto.TodoCreateRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.junit.jupiter.api.Assertions;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

@SpringBootTest
@AutoConfigureMockMvc
public class TodoControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private TodoJPARepository todoJPARepository;

    @BeforeEach
    void setUp() {
        todoJPARepository.deleteAll();
    }

    @Test
    void should_find_todos() throws Exception {
        Todo todo = new Todo();
        todo.setName("Study React");
        todoJPARepository.save(todo);

        mockMvc.perform(get("/todos"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value(todo.getName()));
    }

    @Test
    void should_delete_todo() throws Exception {
        Todo todo = new Todo();
        todo.setName("Study React");
        Todo savedTodo = todoJPARepository.save(todo);

        mockMvc.perform(delete("/todos/{id}", savedTodo.getId()))
                .andExpect(MockMvcResultMatchers.status().is(204));

        assertTrue(todoJPARepository.findById(savedTodo.getId()).isEmpty());
    }

    @Test
    void should_update_todo() throws Exception {
        Todo todo = new Todo();
        todo.setName("Study React");
        Todo savedTodo = todoJPARepository.save(todo);

        Todo updatedTodoRequest = new Todo(savedTodo.getId(), "Study Vue", true);
        ObjectMapper objectMapper = new ObjectMapper();
        String updateTodoJson = objectMapper.writeValueAsString(updatedTodoRequest);

        mockMvc.perform(put("/todos/{id}", savedTodo.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(updateTodoJson))
                .andExpect(MockMvcResultMatchers.status().is(204));
        Optional<Todo> optionalTodo = todoJPARepository.findById(savedTodo.getId());
        assertTrue(optionalTodo.isPresent());
        Todo updatedTodo = optionalTodo.get();
        Assertions.assertEquals(updatedTodoRequest.getId(), updatedTodo.getId());
        Assertions.assertEquals(updatedTodoRequest.getName(), updatedTodo.getName());
        Assertions.assertEquals(updatedTodoRequest.getDone(), updatedTodo.getDone());
    }

    @Test
    void should_create_todo() throws Exception {
        TodoCreateRequest todo = getTodoRequest();
        ObjectMapper objectMapper = new ObjectMapper();
        String todoRequest = objectMapper.writeValueAsString(todo);
        mockMvc.perform(post("/todos")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(todoRequest))
                .andExpect(MockMvcResultMatchers.status().is(201))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(todo.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(todo.getDone()));
    }

    @Test
    void should_find_todo_by_id() throws Exception {
        Todo todo = new Todo();
        todo.setName("Study React");
        Todo savedTodo = todoJPARepository.save(todo);

        mockMvc.perform(get("/todos/{id}", savedTodo.getId()))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(savedTodo.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(todo.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.done").value(todo.getDone()));
    }
    @Test
    void should_throw_not_found_when_findById_given_invalid_id()throws Exception {
        Todo todo = new Todo();
        todo.setName("Study React");
        Todo savedTodo = todoJPARepository.save(todo);

        mockMvc.perform(get("/todo/9999999"))
                .andExpect(MockMvcResultMatchers.status().is(404));
    }


    private static TodoCreateRequest getTodoRequest() {
        Todo todo = new Todo();
        todo.setName("Study React");
        todo.setDone(false);
        TodoCreateRequest todoCreateRequest = new TodoCreateRequest();
        BeanUtils.copyProperties(todo, todoCreateRequest);
        return todoCreateRequest;
    }
}
